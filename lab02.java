public class lab02{
    private static void duplicateZero(int[] arr){
        int zeroes = 0;
        for(int i:arr){
            if(i==0){
                zeroes++;
            }
        }
        
        int i = arr.length-1 , j = arr.length - 1 + zeroes;
        while(i != j){
            insert(arr,i,j--);
            if(arr[i]==0){
                insert(arr,i,j--);
            }
            i--;
        }
    }
    
    private static void insert(int[] arr,int i,int j){
        if(j < arr.length){
            arr[j] = arr[i];
        }
    }

    private static void printArray(int[] arr){
        for (int i =0;i<arr.length;i++){
            System.out.println(arr[i]+" ");
        }
        System.out.println();
    }
    public static void main(String[] args) {
        int[] arr = { 1, 0, 2, 3, 0, 4, 5, 0 };
               
        duplicateZero(arr);
        printArray(arr);
    }
        
            
}
